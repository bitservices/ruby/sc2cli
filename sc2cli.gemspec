###############################################################################

# frozen_string_literal: true

###############################################################################

require_relative 'lib/sc2cli/version'

###############################################################################

Gem::Specification.new do |spec|
  spec.name        = 'sc2cli'
  spec.version     = SC2Cli::VERSION
  spec.authors     = ['Richard Lees']
  spec.email       = ['git0@bitservices.io']

  spec.summary     = 'A command line utility for accessing the StarCraft 2 API written in Ruby.'
  spec.description = 'A command line utility for accessing the StarCraft 2 API written in Ruby. Supports league boundaries, player league details, player match history and more.'
  spec.homepage    = 'https://gitlab.com/bitservices/ruby/sc2cli/'
  spec.license     = 'MIT'

  spec.required_ruby_version = Gem::Requirement.new('>= 2.6.0')

  spec.metadata['homepage_uri']    = spec.homepage
  spec.metadata['source_code_uri'] = spec.homepage

  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{\A(?:archlinux|test|spec|features)/}) or f.match(%r{\Aavatar(?:_group)?\.png\z}) }
  end

  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'rspec', '~> 3.10'
end

###############################################################################
