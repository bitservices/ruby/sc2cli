###############################################################################

module SC2Cli

###############################################################################

  module Subcommands

###############################################################################

    module LadderShared

###############################################################################

      class LadderSummary

###############################################################################

        @ladders

###############################################################################

        def initialize(json:, api:, player:)
          @ladders = Array.new

          if json.key?("allLadderMemberships") then
            ladders = json["allLadderMemberships"]
            @@console.fatal("Returned ladder summary list of all ladders is not an array!") unless ladders.kind_of?(Array)

            ladders.each do |ladder|
              @@console.fatal("Returned ladder summary list of all ladders contains a ladder without an ID!") unless ladder.key?("ladderId")

              id = ladder["ladderId"]

              id = id.to_i if id.kind_of?(String)

              ladder = LadderDetails.new(id: id, api: api, player: player)
              add(ladder: ladder)
            end
          end
        end

###############################################################################

        def add(ladder:)
          id = ladder.id

          unless @ladders.any? {|ladder| ladder.id == id}
            @ladders << ladder if ladder.kind_of?(LadderDetails)
            @ladders.sort_by!{ |ladder| ladder.membership.type }
          end
        end

###############################################################################

        def to_s
          result = String.new

          @ladders.each do |ladder|
            result += ladder.to_s
          end

          return result
        end

###############################################################################

      end

###############################################################################

    end

###############################################################################

  end

###############################################################################

end

###############################################################################
