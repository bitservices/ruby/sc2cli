###############################################################################

module SC2Cli

###############################################################################

  module Subcommands

###############################################################################

    module LadderShared

###############################################################################

      class LadderDetailsMembership

###############################################################################

        @@console = Shared::Console.instance

###############################################################################

        attr_reader :type

###############################################################################

        def initialize(json:)
          @@console.fatal("Failed to get ladder membership details: game mode / type missing!") unless json.key?("localizedGameMode")

          type = json["localizedGameMode"]

          @@console.fatal("Failed to get ladder membership details: game mode / type is not a string!") unless type.kind_of?(String)
          @@console.fatal("Failed to get ladder membership details: game mode / type is blank!") if type.empty?

          @type = type
        end

###############################################################################

        def to_s
          result = String.new

          result = "Type: #{@type}\n"

          return result
        end

###############################################################################

      end

###############################################################################

    end

###############################################################################

  end

###############################################################################

end

###############################################################################
