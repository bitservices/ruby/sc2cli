###############################################################################

require 'yaml'

###############################################################################

module SC2Cli

###############################################################################

  module Shared

###############################################################################

    class Configuration

###############################################################################

      @@console = Console.instance

###############################################################################

      @@file   = "configuration.yaml"
      @@folder = ".sc2cli"

###############################################################################

      attr_reader :base
      attr_reader :client
      attr_reader :region
      attr_reader :secret

###############################################################################

      def initialize
        home = Dir.home

        @@console.fatal("Home folder could not be determined!") if home.empty?
        @@console.fatal("Home folder: #{home} does not exist!") unless File.directory?(home)

        base = File.join(home, @@folder)

        @@console.fatal("Base folder: #{base} does not exist!") unless File.directory?(base)

        @base = base

        path = File.join(base, @@file)

        load(path: path)
      end

###############################################################################

      def auth
        result = false

        if not (client.nil? or secret.nil?) then
          result = true
        end

        return result
      end

###############################################################################

      private

###############################################################################

      def load(path:)
        client = nil
        secret = nil
        region = nil

        if File.file?(path)
          @@console.info("Reading configuration: #{path}")
          yaml = YAML.load(File.read(path))

          if yaml.key?("client") then
            client = yaml["client"]

            @@console.fatal("Error in configuration! 'client' must be a string!") unless client.kind_of?(String)
            @@console.fatal("Error in configuration! 'client' must not be blank!") if client.empty?

            @@console.fatal("Error in configuration! 'client' set but 'secret' missing!") unless yaml.key?("secret")

            secret = yaml["secret"]

            @@console.fatal("Error in configuration! 'secret' must be a string!") unless secret.kind_of?(String)
            @@console.fatal("Error in configuration! 'secret' must not be blank!") if secret.empty?
          end

          if yaml.key?("region") then
            region_name = yaml["region"]

            @@console.fatal("Error in configuration! 'region' must be a string!") unless region.kind_of?(String)
            @@console.fatal("Error in configuration! 'region' must not be blank!") if region.empty?
          end
        end

        @region = Region.new(name: region)

        @client = client
        @secret = secret
      end

###############################################################################

    end

###############################################################################

  end

###############################################################################

end

###############################################################################
