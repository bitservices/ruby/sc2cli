###############################################################################

require 'json'
require 'net/http'

###############################################################################

module SC2Cli

###############################################################################

  module Shared

###############################################################################

    class Token

###############################################################################

      @@console = Console.instance

###############################################################################

      @@cutoff = 3600

###############################################################################

      @cache
      @configuration

###############################################################################

      attr_reader :expires
      attr_reader :region
      attr_reader :token

###############################################################################

      def initialize(configuration:, region: nil)
        @configuration = configuration
        @region        = region || @configuration.region

        @@console.info("Finding token for region: #{@region.name} (#{@region.description})")

        @cache = Cache.new(configuration: @configuration, region: @region)

        begin
          from_cache
        rescue
          refresh
        end
      end

###############################################################################

      def check
        return valid(expires: @expires)
      end

###############################################################################

      def refresh
        from_server
        @cache.update(token: @token, expires: @expires)
      end

###############################################################################

      private

###############################################################################

      def from_cache
        raise "Cache 'token' or 'expires' objects are not set!" if @cache.token.nil? or @cache.expires.nil?
        raise "Cached token has already expired!" unless valid(expires: @cache.expires)

        @token   = @cache.token
        @expires = @cache.expires
      end

###############################################################################

      def from_server
        @@console.fatal("No client/secret in configuration and no usable token in cache!") unless @configuration.auth

        server = @region.oauth_server
        @@console.info("Refreshing token from: #{server}")

        uri     = URI("https://#{server}/oauth/token")
        request = Net::HTTP::Post.new(uri)

        request.basic_auth(@configuration.client, @configuration.secret)
        request.set_form_data("grant_type" => "client_credentials")

        http = Net::HTTP.new(uri.hostname, uri.port)
        http.use_ssl = true

        response = http.request(request)

        @@console.fatal("Blizzard OAuth did not return status 200 for token reqest!") if response.code != "200"
        @@console.fatal("Response body from Blizzard OAuth is not permitted!")        if not response.class.body_permitted?
        @@console.fatal("Response body from Blizzard OAuth is empty!")                if response.body.nil?

        body = JSON.parse(response.body)

        @@console.fatal("Blizzard OAuth JSON response did not include an access token!") if not body.key?("access_token")
        @@console.fatal("Blizzard OAuth gave a blank access token!")                     if body["access_token"].empty?

        @@console.fatal("Blizzard OAuth JSON response did not include an expiry time!") if not body.key?("expires_in")

        token      = body["access_token"]
        expires_in = body["expires_in"]
        expires    = Time.now + expires_in

        @@console.fatal("Token received from Blizzard appears to have already expired!") unless valid(expires: expires)

        @token   = token
        @expires = expires
      end

###############################################################################

      def valid(expires:, cutoff: @@cutoff)
        return (expires - @@cutoff) > Time.now
      end

###############################################################################

    end

###############################################################################

  end

###############################################################################

end

###############################################################################
