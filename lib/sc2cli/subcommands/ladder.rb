###############################################################################

require 'optparse'

###############################################################################

require_relative 'ladder/ladderdetails'
require_relative 'ladder/ladderdetailsmembership'
require_relative 'ladder/ladderdetailsrank'
require_relative 'ladder/ladderdetailsteam'
require_relative 'ladder/ladderdetailsteammembers'
require_relative 'ladder/ladderdetailsteammember'
require_relative 'ladder/ladderdetailsteams'
require_relative 'ladder/laddersummary'

###############################################################################

module SC2Cli

###############################################################################

  module Subcommands

###############################################################################

    class Ladder

###############################################################################

      @@console = Shared::Console.instance

###############################################################################

      @@prefix = "/sc2/profile"
      @@suffix = "/ladder/summary"

###############################################################################

      @configuration

###############################################################################

      @id
      @region

###############################################################################

      def initialize(configuration:, options:)
        @configuration = configuration

        id     = nil
        name   = nil
        region = nil

        OptionParser.new do |opts|
          opts.banner = "Usage: #{$0} #{self.class.name.split("::").last.downcase} [options]"

          opts.on("-h", "--help", "Prints this help") do
            @@console.info(opts)
            exit
          end

          opts.on("-i", "--id ID", Integer, "Player profile ID.") do |value|
            id = value
          end

          opts.on("-n", "--name NAME", String, "Player name. Must exist in names file.") do |value|
            name = value
          end

          opts.on("-r", "--region REGION", String, "Region name, such as 'eu' or 'us'. Use configuration region by default.") do |value|
            region = Shared::Region.new(name: value)
          end
        end.parse!

        if name.kind_of?(String) then
          name     = Shared::Names.new(configuration: @configuration, name: name)
          id       = name.id
          region ||= name.region
        end

        region ||= @configuration.region

        @@console.fatal("Player profile ID or name must be specified!") unless id.kind_of?(Integer)

        @id     = id
        @region = region
      end

###############################################################################

      def run
        @@console.info("Finding ladder summary:")
        @@console.info(" - ID    : #{@id.to_s}")
        @@console.info(" - Region: #{@region.description}")

        path = "#{@@prefix}/#{@region.id.to_s}/#{@region.realm.to_s}/#{@id.to_s}#{@@suffix}"

        api = Shared::Api.new(configuration: @configuration, region: @region)

        result = api.get(path: path)

        ladders = LadderShared::LadderSummary.new(json: result, api: api, player: @id)

        @@console.info(ladders.to_s)
      end

###############################################################################

    end

###############################################################################

  end

###############################################################################

end

###############################################################################
