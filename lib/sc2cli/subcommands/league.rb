###############################################################################

require 'optparse'

###############################################################################

require_relative 'league/leaguetier'
require_relative 'league/leaguetiers'

###############################################################################

module SC2Cli

###############################################################################

  module Subcommands

###############################################################################

    class League

###############################################################################

      @@console = Shared::Console.instance

###############################################################################

      @@prefix = "/data/sc2/league"
      @@type   = "1v1"
      @@random = false

###############################################################################

      @@valid_codes = {
        "b"           => "bronze",
        "bronze"      => "bronze",
        "s"           => "silver",
        "silver"      => "silver",
        "g"           => "gold",
        "gold"        => "gold",
        "p"           => "platinum",
        "plat"        => "platinum",
        "platinum"    => "platinum",
        "d"           => "diamond",
        "diamond"     => "diamond",
        "m"           => "master",
        "master"      => "master",
        "gm"          => "grandmaster",
        "grandmaster" => "grandmaster"
      }

      @@valid_names = {
        "bronze"      => { "colour" => 172, "value" => 0 },
        "silver"      => { "colour" => 247, "value" => 1 },
        "gold"        => { "colour" => 220, "value" => 2 },
        "platinum"    => { "colour" => 75 , "value" => 3 },
        "diamond"     => { "colour" => 27 , "value" => 4 },
        "master"      => { "colour" => 45 , "value" => 5 },
        "grandmaster" => { "colour" => 202, "value" => 6 }
      }

      @@valid_types = {
        "1v1" => { "random" => false, "value" => 201 },
        "2v2" => { "random" => true , "value" => 202 },
        "3v3" => { "random" => true , "value" => 203 },
        "4v4" => { "random" => true , "value" => 204 }
      }

###############################################################################

      @configuration

###############################################################################

      @name
      @random
      @region
      @season
      @type

###############################################################################

      def initialize(configuration:, options:)
        @configuration = configuration

        name   = nil
        random = nil
        region = nil
        season = nil
        type   = nil

        OptionParser.new do |opts|
          opts.banner = "Usage: #{$0} #{self.class.name.split("::").last.downcase} [options]"

          opts.on("-h", "--help", "Prints this help") do
            @@console.info(opts)
            exit
          end

          opts.on("-n", "--name NAME", String, "League name, such as 'gold' or 'silver'.") do |value|
            name = value
          end

          opts.on("-r", "--region REGION", String, "Region name, such as 'eu' or 'us'. Use configuration region by default.") do |value|
            region = Shared::Region.new(name: value)
          end

          opts.on("-R", "--[no-]random", "Specify for randomly assigned teams. Default is arranged teams.") do |value|
            random = value
          end

          opts.on("-s", "--season SEASON", Integer, "Season number.") do |value|
            season = value
          end

          opts.on("-t", "--type TYPE", String, "League type, such as '1v1' or '2v2'. Default is '1v1'.") do |value|
            type = value
          end
        end.parse!

        random ||= @@random
        region ||= @configuration.region
        type   ||= @@type

        @@console.fatal("League name must be specified!") unless name.kind_of?(String)

        @@console.fatal("Specified league name: #{name} is not valid!") unless @@valid_codes.key?(name)
        @@console.fatal("Specified league type: #{type} is not valid!") unless @@valid_types.key?(type)

        name = @@valid_codes[name]

        @@console.fatal("Leagues name resolved from code: #{name} is not valid! This should never happen...") unless @@valid_names.key?(name)

        if random then
          @@console.fatal("There are no randomly assigned teams for leagues of type: #{type}!") unless @@valid_types[type]["random"]
        end

        unless season.kind_of?(Integer) then
          current_season = Shared::Season.new(configuration: @configuration, region: region)
          season = current_season.id
        end

        @name   = name
        @random = random
        @region = region
        @season = season
        @type   = type
      end

###############################################################################

      def run
        @@console.info("Finding league information:")
        @@console.info(" - Name  : #{@name}")
        @@console.info(" - Random: #{@random.to_s}")
        @@console.info(" - Region: #{@region.description}")
        @@console.info(" - Season: #{@season.to_s}")
        @@console.info(" - Type  : #{@type}")

        api_name   = @@valid_names[@name]["value"].to_s
        api_random = @random ? "1" : "0"
        api_season = @season.to_s
        api_type   = @@valid_types[@type]["value"].to_s

        colour = @@valid_names[@name]["colour"]

        path = "#{@@prefix}/#{api_season}/#{api_type}/#{api_random}/#{api_name}"

        api = Shared::Api.new(configuration: @configuration, region: @region)

        result = api.get(path: path)

        tiers = LeagueShared::LeagueTiers.new(name: @name, colour: colour, json: result)

        @@console.info(tiers.to_s)
      end

###############################################################################

    end

###############################################################################

  end

###############################################################################

end

###############################################################################
