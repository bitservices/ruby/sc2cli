###############################################################################

require_relative 'shared/console'

###############################################################################

require_relative 'shared/api'
require_relative 'shared/cache'
require_relative 'shared/configuration'
require_relative 'shared/names'
require_relative 'shared/region'
require_relative 'shared/season'
require_relative 'shared/token'

###############################################################################
