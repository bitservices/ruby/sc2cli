###############################################################################

module SC2Cli

###############################################################################

  module Subcommands

###############################################################################

    module LadderShared

###############################################################################

      class LadderDetailsTeamMember

###############################################################################

        @@console = Shared::Console.instance

###############################################################################

        @@race_colour_default = 247

        @@race_colour = {
          "protoss" => 220,
          "random"  => 76,
          "terran"  => 39,
          "zerg"    => 99
        }

###############################################################################

        attr_reader :clan
        attr_reader :id
        attr_reader :name
        attr_reader :race

###############################################################################

        def initialize(json:)
          @@console.fatal("Failed to get ladder team member details: ID missing!") unless json.key?("id")

          id = json["id"]

          id = id.to_i if id.kind_of?(String)

          @@console.fatal("Failed to get ladder team member details: ID is not an integer!") unless id.kind_of?(Integer)
          @@console.fatal("Failed to get ladder team member details: ID is not valid!") unless id > 0

          clan = "None"
          name = "Unknown"
          race = "Unknown"

          if json.key?("clanTag") then
            if json["clanTag"].kind_of?(String)
              clan = json["clanTag"] unless json["clanTag"].empty?
            end
          end

          if json.key?("displayName") then
            if json["displayName"].kind_of?(String)
              name = json["displayName"] unless json["displayName"].empty?
            end
          end

          if json.key?("favoriteRace") then
            if json["favoriteRace"].kind_of?(String)
              race = json["favoriteRace"] unless json["favoriteRace"].empty?
            end
          end

          @clan = clan
          @id   = id
          @name = name
          @race = race
        end

###############################################################################

        def name_with_clan
          result = clan == "None" ? name : "[#{clan}]#{name}"
        end

###############################################################################

        def to_s
          colour = @@race_colour.key?(race) ? @@race_colour[race] : @@race_colour_default
          race   = "%-10.10s" % @race
          result = "#{@@console.format(colour: colour, message: race)}#{name_with_clan}\n"
          return result
        end

###############################################################################

      end

###############################################################################

    end

###############################################################################

  end

###############################################################################

end

###############################################################################
