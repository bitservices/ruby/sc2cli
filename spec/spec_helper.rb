###############################################################################

require_relative '../lib/sc2cli'

###############################################################################

RSpec.configure do |config|

###############################################################################

  config.before {
    allow_any_instance_of(IO).to receive(:puts).and_return(nil)
  }

###############################################################################

  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

###############################################################################

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

###############################################################################

  config.shared_context_metadata_behavior = :apply_to_host_groups

###############################################################################

end

###############################################################################
