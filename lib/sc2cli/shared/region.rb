###############################################################################

module SC2Cli

###############################################################################

  module Shared

###############################################################################

    class Region

###############################################################################

      @@console = Console.instance

###############################################################################

      @@name = "eu"

###############################################################################

      @@regions = {
        "cn" => {
          "api_server"   => "gateway.battlenet.com.cn",
          "description"  => "China",
          "id"           => 5,
          "oauth_server" => "www.battlenet.com.cn",
          "realm"        => 1
        },
        "eu" => {
          "api_server"   => "eu.api.blizzard.com",
          "description"  => "Europe",
          "id"           => 2,
          "oauth_server" => "eu.battle.net",
          "realm"        => 1
        },
        "la" => {
          "api_server"   => "us.api.blizzard.com",
          "description"  => "Latin America",
          "id"           => 1,
          "oauth_server" => "us.battle.net",
          "realm"        => 2
        },
        "kr" => {
          "api_server"   => "kr.api.blizzard.com",
          "description"  => "Korea",
          "id"           => 3,
          "oauth_server" => "kr.battle.net",
          "realm"        => 1
        },
        "ru" => {
          "api_server"   => "eu.api.blizzard.com",
          "description"  => "Russia",
          "id"           => 2,
          "oauth_server" => "eu.battle.net",
          "realm"        => 2
        },
        "tw" => {
          "api_server"   => "tw.api.blizzard.com",
          "description"  => "Taiwan",
          "id"           => 3,
          "oauth_server" => "apac.battle.net",
          "realm"        => 2
        },
        "us" => {
          "api_server"   => "us.api.blizzard.com",
          "description"  => "United States",
          "id"           => 1,
          "oauth_server" => "us.battle.net",
          "realm"        => 1
        }
      }

###############################################################################

      attr_reader :api_server
      attr_reader :description
      attr_reader :id
      attr_reader :name
      attr_reader :oauth_server
      attr_reader :realm

###############################################################################

      def initialize(name: nil)
        name = name || @@name

        @@console.fatal("Invalid region: #{name}!") unless @@regions.key?(name)

        @name = name

        @api_server   = @@regions[name]["api_server"]
        @description  = @@regions[name]["description"]
        @id           = @@regions[name]["id"]
        @oauth_server = @@regions[name]["oauth_server"]
        @realm        = @@regions[name]["realm"]
      end

###############################################################################

    end

###############################################################################

  end

###############################################################################

end

###############################################################################
