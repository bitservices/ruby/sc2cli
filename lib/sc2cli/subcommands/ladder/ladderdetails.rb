###############################################################################

module SC2Cli

###############################################################################

  module Subcommands

###############################################################################

    module LadderShared

###############################################################################

      class LadderDetails

###############################################################################

        @@console = Shared::Console.instance

###############################################################################

        @@prefix = "/sc2/profile"
        @@suffix = "/ladder"

###############################################################################

        @teams

###############################################################################

        attr_reader :id
        attr_reader :membership
        attr_reader :player
        attr_reader :rank
        attr_reader :region

###############################################################################

        def initialize(id:, api:, player:)
          @@console.fatal("Cannot find ladder details for ladder with an ID that is not an integer!") unless id.kind_of?(Integer)
          @@console.fatal("Cannot find ladder details for ladder with an ID that is not valid!") unless id > 0

          @id     = id
          @player = player
          @region = api.region

          lookup(api: api)
        end

###############################################################################

        def to_s
          result = String.new

          result += "-------------------------------------------------------------------------------\n"
          result += @membership.to_s
          result += "-------------------------------------------------------------------------------\n"
          result += @teams.with_member(player: @player).to_s
          result += "Ranks/Pools for this player:\n"
          result += @rank.to_s

          return result
        end

###############################################################################

        private

###############################################################################

        def lookup(api:)
          @@console.info("Finding ladder details:")
          @@console.info(" - ID    : #{@id.to_s}")
          @@console.info(" - Player: #{@player.to_s}")
          @@console.info(" - Region: #{@region.description}")

          path = "#{@@prefix}/#{@region.id.to_s}/#{@region.realm.to_s}/#{@player.to_s}#{@@suffix}/#{@id.to_s}"

          result = api.get(path: path)

          @@console.fatal("Failed to get ladder details: current membership information missing!") unless result.key?("currentLadderMembership")

          membership = result["currentLadderMembership"]

          rank  = result.key?("ranksAndPools") ? result["ranksAndPools"] : Array.new
          teams = result.key?("ladderTeams")   ? result["ladderTeams"]   : Array.new

          @membership = LadderDetailsMembership.new(json: membership)
          @rank       = LadderDetailsRank.new(json: rank)
          @teams      = LadderDetailsTeams.new(json: teams)
        end

###############################################################################

      end

###############################################################################

    end

###############################################################################

  end

###############################################################################

end

###############################################################################
