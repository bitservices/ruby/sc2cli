###############################################################################

module SC2Cli

###############################################################################

  module Subcommands

###############################################################################

    module HistoryShared

###############################################################################

      class HistoryMatch

###############################################################################

        @@console = Shared::Console.instance

###############################################################################

        @@decision_colour_default = 247

        @@decision_colour = {
          "Win"      => 40,
          "Loss"     => 160,
          "Left"     => 184,
          "Observer" => 33
        }

###############################################################################

        @@speed = "Unknown"

###############################################################################

        attr_reader :date
        attr_reader :descision
        attr_reader :map
        attr_reader :speed
        attr_reader :type

###############################################################################

        def initialize(json:)
          @@console.fatal("Returned history information has a match with a missing date/time!") unless json.key?("date")
          @@console.fatal("Returned history information has a match with a missing decision!") unless json.key?("decision")
          @@console.fatal("Returned history information has a match with a missing map!") unless json.key?("map")
          @@console.fatal("Returned history information has a match with a missing type!") unless json.key?("type")

          date = json["date"]

          @@console.fatal("Returned history information has a match with a date/time that is not an integer!") unless date.kind_of?(Integer)
          @@console.fatal("Returned history information has a match with a date/time that is invalid!") unless date >= 0

          decision = json["decision"]

          @@console.fatal("Returned history information has a match with a decision that is not a string!") unless decision.kind_of?(String)
          @@console.fatal("Returned history information has a match with a decision that is blank!") if decision.empty?

          map = json["map"]

          @@console.fatal("Returned history information has a match with a map that is not a string!") unless map.kind_of?(String)
          @@console.fatal("Returned history information has a match with a map that is blank!") if map.empty?

          type = json["type"]

          @@console.fatal("Returned history information has a match with a type that is not a string!") unless type.kind_of?(String)
          @@console.fatal("Returned history information has a match with a type that is blank!") if type.empty?

          @date     = Time.at(date)
          @decision = decision
          @map      = map
          @type     = type

          if json.key?("speed") then
            speed = json["speed"]
            if speed.kind_of?(String) then
              @speed = speed unless speed.empty?
            end
          end

          @speed ||= @@speed
        end

###############################################################################

        def to_s
          result = String.new

          colour   = @@decision_colour.key?(@decision) ? @@decision_colour[@decision] : @@decision_colour_default
          date     = @date.strftime("%Y-%m-%d %H:%M:%S")
          decision = "%10.10s" % @decision
          map      = "%-36.36s" % @map
          type     = "%10.10s" % @type

          result = "#{@@console.format(colour: colour, message: decision)} #{type} #{map} #{date}\n"
          return result
        end

###############################################################################

      end

###############################################################################

    end

###############################################################################

  end

###############################################################################

end

###############################################################################
