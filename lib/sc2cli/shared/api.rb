###############################################################################

require 'json'
require 'net/http'

###############################################################################

module SC2Cli

###############################################################################

  module Shared

###############################################################################

    class Api

###############################################################################

      @@console = Console.instance

###############################################################################

      @token

###############################################################################

      attr_reader :region

###############################################################################

      def initialize(configuration:, region: nil)
        @region = region || configuration.region
        @token  = Token.new(configuration: configuration, region: @region)
      end

###############################################################################

      def get(path: "/")
        @@console.fatal("Path for API request cannot be blank!") if path.empty?
        @@console.fatal("Path for API request: #{path} does NOT begin with '/'!") unless path.chars.first == "/"

        server = @region.api_server

        @token.refresh unless @token.check

        uri = URI("https://#{server}#{path}")

        @@console.info("Making API request to: #{uri.to_s}")

        request = Net::HTTP::Get.new(uri)
        request["Authorization"] = "Bearer #{@token.token}"

        http = Net::HTTP.new(uri.hostname, uri.port)
        http.use_ssl = true

        response = http.request(request)

        @@console.fatal("Blizzard API returned: #{response.code}, not 200 for reqest!") if response.code != "200"
        @@console.fatal("Response body from Blizzard API is not permitted!")            if not response.class.body_permitted?
        @@console.fatal("Response body from Blizzard API is empty!")                    if response.body.nil?

        body = JSON.parse(response.body)
        return body
      end

###############################################################################

    end

###############################################################################

  end

###############################################################################

end

###############################################################################
