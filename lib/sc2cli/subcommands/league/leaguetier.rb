###############################################################################

module SC2Cli

###############################################################################

  module Subcommands

###############################################################################

    module LeagueShared

###############################################################################

      class LeagueTier

###############################################################################

        @@console = Shared::Console.instance

###############################################################################

        @@mmr_min = 0
        @@mmr_max = 0

###############################################################################

        @mmr_min
        @mmr_max

###############################################################################

        attr_reader :colour
        attr_reader :id
        attr_reader :league
        attr_reader :number

###############################################################################

        def initialize(league:, colour:, json:)
          @@console.fatal("Returned league information for: #{league} has a tier with a missing ID!") unless json.key?("id")

          id = json["id"]

          @@console.fatal("Returned league information for: #{league} has a tier with an ID that is not an integer!") unless id.kind_of?(Integer)
          @@console.fatal("Returned league information for: #{league} has a tier with an ID that is invalid!") unless id >= 0

          @id     = id
          @league = league
          @number = id + 1
          @colour = colour

          if json.key?("min_rating") then
            mmr_min = json["min_rating"]
            @mmr_min = mmr_min if mmr_min.kind_of?(Integer)
          end

          if json.key?("max_rating") then
            mmr_max = json["max_rating"]
            @mmr_max = mmr_max if mmr_max.kind_of?(Integer)
          end

          @mmr_min ||= @@mmr_min
          @mmr_max ||= @@mmr_max
        end

###############################################################################

        def lower
          return @mmr_min
        end

###############################################################################

        def upper
          return @mmr_max
        end

###############################################################################

        def to_s
          result = String.new

          mmr_min = @mmr_min > 0 ? @mmr_min.to_s : "Unknown"
          mmr_max = @mmr_max > 0 ? @mmr_max.to_s : "Unknown"

          result += "-------------------------------------------------------------------------------\n"
          result += "League: #{@@console.format(colour: @colour, message: "#{@league} #{@number}")}\n"
          result += "-------------------------------------------------------------------------------\n"
          result += "Maximum MMR: #{@@console.format(colour: @colour, message: mmr_max)}\n"
          result += "Minimum MMR: #{@@console.format(colour: @colour, message: mmr_min)}\n\n"

          return result
        end

###############################################################################

      end

###############################################################################

    end

###############################################################################

  end

###############################################################################

end

###############################################################################
