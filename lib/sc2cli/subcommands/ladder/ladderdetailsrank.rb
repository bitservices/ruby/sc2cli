###############################################################################

module SC2Cli

###############################################################################

  module Subcommands

###############################################################################

    module LadderShared

###############################################################################

      class LadderDetailsRank

###############################################################################

        @@console = Shared::Console.instance

###############################################################################

        attr_reader :bonus
        attr_reader :mmr
        attr_reader :rank

###############################################################################

        def initialize(json:)
          bonus = 0
          mmr   = 0
          rank  = 0

          @@console.warn("Ladder details contains multiple rank/pool details. Will ignore all but the first!") if json.length > 1

          result = json[0]

          if result.key?("bonusPool") then
            bonus = result["bonusPool"] if result["bonusPool"].kind_of?(Integer)
          end

          if result.key?("mmr") then
            mmr = result["mmr"] if result["mmr"].kind_of?(Integer)
          end

          if result.key?("rank") then
            rank = result["rank"] if result["rank"].kind_of?(Integer)
          end

          @bonus = bonus
          @mmr   = mmr
          @rank  = rank
        end

###############################################################################

        def bonus_s
          return bonus.to_s
        end

###############################################################################

        def mmr_s
          result = mmr > 0 ? mmr.to_s : "Unknown"
          return result
        end

###############################################################################

        def rank_s
          result = rank > 0 ? rank.to_s : "Unknown"
          return result
        end

###############################################################################

        def to_s
          result = String.new

          result = " - MMR: #{mmr_s}, Bonus Pool: #{bonus_s}, Rank: #{rank_s}\n"

          return result
        end

###############################################################################

      end

###############################################################################

    end

###############################################################################

  end

###############################################################################

end

###############################################################################
