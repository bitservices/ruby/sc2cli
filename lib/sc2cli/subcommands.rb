###############################################################################

require_relative 'subcommands/history'
require_relative 'subcommands/ladder'
require_relative 'subcommands/league'
require_relative 'subcommands/raw'
require_relative 'subcommands/season'

###############################################################################
