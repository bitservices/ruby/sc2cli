###############################################################################

require 'optparse'

###############################################################################

module SC2Cli

###############################################################################

  module Subcommands

###############################################################################

    class Season

###############################################################################

      @@console = Shared::Console.instance

###############################################################################

      @configuration
      @region

###############################################################################

      def initialize(configuration:, options:)
        @configuration = configuration

        region = nil

        OptionParser.new do |opts|
          opts.banner = "Usage: #{$0} #{self.class.name.split("::").last.downcase} [options]"

          opts.on("-h", "--help", "Prints this help") do
            @@console.info(opts)
            exit
          end

          opts.on("-r", "--region REGION", String, "Region name, such as 'eu' or 'us'. Use configuration region by default.") do |value|
            region = Shared::Region.new(name: value)
          end
        end.parse!

        region ||= @configuration.region

        @region = region
      end

###############################################################################

      def run
        @@console.info("Running season API request:")
        @@console.info(" - Region: #{@region.description}")

        season = Shared::Season.new(configuration: @configuration, region: @region)

        @@console.info(season.to_s)
      end

###############################################################################

    end

###############################################################################

  end

###############################################################################

end

###############################################################################
