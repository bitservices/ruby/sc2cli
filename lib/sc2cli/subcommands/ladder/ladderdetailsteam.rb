###############################################################################

module SC2Cli

###############################################################################

  module Subcommands

###############################################################################

    module LadderShared

###############################################################################

      class LadderDetailsTeam

###############################################################################

      @@console = Shared::Console.instance

###############################################################################

      @@colour_wins   = 40
      @@colour_losses = 160

###############################################################################

        attr_reader :joined
        attr_reader :losses
        attr_reader :members
        attr_reader :mmr
        attr_reader :points
        attr_reader :previous
        attr_reader :wins

###############################################################################

        def initialize(json:)
          joined   = 0
          losses   = 0
          members  = Array.new
          mmr      = 0
          points   = 0
          previous = 0
          wins     = 0

          if json.key?("joinTimestamp") then
            joined = json["joinTimestamp"] if json["joinTimestamp"].kind_of?(Integer)
          end

          if json.key?("losses") then
            losses = json["losses"] if json["losses"].kind_of?(Integer)
          end

          if json.key?("teamMembers") then
            members = json["teamMembers"] if json["teamMembers"].kind_of?(Array)
          end

          if json.key?("mmr") then
            mmr = json["mmr"] if json["mmr"].kind_of?(Integer)
          end

          if json.key?("points") then
            points = json["points"] if json["points"].kind_of?(Integer)
          end

          if json.key?("previousRank") then
            previous = json["previousRank"] if json["previousRank"].kind_of?(Integer)
          end

          if json.key?("wins") then
            wins = json["wins"] if json["wins"].kind_of?(Integer)
          end

          @members  = LadderDetailsTeamMembers.new(json: members)
          @joined   = Time.at(joined)

          @losses   = losses
          @mmr      = mmr
          @points   = points
          @previous = previous
          @wins     = wins
        end

###############################################################################

        def to_s
          result = String.new

          joined = @joined.strftime("%Y-%m-%d %H:%M:%S")

          result += @members.to_s
          result += "MMR: #{@mmr.to_s}, Points: #{@points.to_s}, Wins: #{@@console.format(colour: @@colour_wins, message: @wins.to_s)}, Losses: #{@@console.format(colour: @@colour_losses, message: @losses.to_s)}, Joined: #{joined}\n\n"

          return result
        end

###############################################################################

      end

###############################################################################

    end

###############################################################################

  end

###############################################################################

end

###############################################################################
