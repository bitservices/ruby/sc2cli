###############################################################################

shared_examples 'valid region given' do |name|
  describe "passing #{name} as region" do
    it 'must return valid data and not exit' do
      begin
        if region_name.nil? then
          region = SC2Cli::Shared::Region.new
        else
          region = SC2Cli::Shared::Region.new(name: region_name)
        end
      rescue SystemExit => e
        raise('Test Failed!')
      end
 
      expect(region.api_server).to be_kind_of(String)
      expect(region.api_server).to_not be_empty

      expect(region.description).to be_kind_of(String)
      expect(region.description).to_not be_empty

      expect(region.id).to be_kind_of(Integer)

      expect(region.oauth_server).to be_kind_of(String)
      expect(region.oauth_server).to_not be_empty
    end
  end
end

###############################################################################

describe SC2Cli::Shared::Region do
  before(:all) do
    @region_valid   = 'us'
    @region_invalid = 'zz'
  end

  describe 'invalid region given' do
    it 'must exit' do
      expect { SC2Cli::Shared::Region.new(name: @region_invalid) }.to raise_error(SystemExit)
    end

    it 'must have error code 1' do
      begin
        SC2Cli::Shared::Region.new(name: @region_invalid)
      rescue SystemExit => e
        expect(e.status).to eq(1)
      end
    end
  end

  it_behaves_like 'valid region given', 'specified region' do
    let(:region_name) { @region_valid }
  end

  it_behaves_like 'valid region given', 'default region' do
    let(:region_name) { nil }
  end
end

###############################################################################
