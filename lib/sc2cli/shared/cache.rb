###############################################################################

require 'yaml'

###############################################################################

module SC2Cli

###############################################################################

  module Shared

###############################################################################

    class Cache

###############################################################################

      @@console = Console.instance

###############################################################################

      @@prefix = "cache."
      @@suffix = ".yaml"

###############################################################################

      @path

###############################################################################

      attr_reader :base
      attr_reader :expires
      attr_reader :region
      attr_reader :token

###############################################################################

      def initialize(configuration:, region: nil)
        @region = region || configuration.region
        @base   = configuration.base

        @path = File.join(@base, "#{@@prefix}#{region.name}#{@@suffix}")

        load
      end

###############################################################################

      def update(token:, expires:)
        @token   = token
        @expires = expires

        save
      end

###############################################################################

      private

###############################################################################

      def load
        token   = nil
        expires = nil

        if File.file?(@path)
          @@console.info("Reading cache: #{@path}")
          yaml = YAML.load(File.read(@path))

          if yaml.key?("token") then
            token = yaml["token"]

            @@console.fatal("Error in cache! 'token' must be a string!") unless token.kind_of?(String)
            @@console.fatal("Error in cache! 'token' must not be blank!") if token.empty?

            @@console.fatal("Error in cache! 'token' set but 'expires' missing!") unless yaml.key?("expires")

            expires = yaml["expires"]

            @@console.fatal("Error in cache! 'expires' must be a integer!") unless expires.kind_of?(Integer)
          end
        end

        @token   = token
        @expires = expires.kind_of?(Integer) ? Time.at(expires) : nil
      end

###############################################################################

      def save
        yaml = Hash.new

        yaml["token"]   = @token
        yaml["expires"] = @expires.to_i

        File.write(@path, yaml.to_yaml)
      end

###############################################################################

    end

###############################################################################

  end

###############################################################################

end

###############################################################################
