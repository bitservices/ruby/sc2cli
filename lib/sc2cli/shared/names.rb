###############################################################################

require 'yaml'

###############################################################################

module SC2Cli

###############################################################################

  module Shared

###############################################################################

    class Names

###############################################################################

      @@console = Console.instance

###############################################################################

      @@file = "names.yaml"

###############################################################################

      @configuration
      @path

###############################################################################

      attr_reader :base
      attr_reader :id
      attr_reader :name
      attr_reader :region

###############################################################################

      def initialize(configuration:, name:)
        @configuration = configuration
        @base          = @configuration.base

        @path = File.join(@base, @@file)

        load(name: name)
      end

###############################################################################

      private

###############################################################################

      def load(name:)
        id      = nil
        region  = nil

        @@console.fatal("Player names file: #{@path} not found!") unless File.file?(@path)

        @@console.info("Reading player names file: #{@path}")
        yaml = YAML.load(File.read(@path))

        @@console.fatal("Player: #{name} not found in names file!") unless yaml.key?(name)

        player = yaml[name]

        @@console.fatal("Player: #{name} is not stored as a valid hash in the names file!") unless player.kind_of?(Hash)
        @@console.fatal("Player: #{name} has no ID associated with it!") unless player.key?("id")

        id = player["id"]

        @@console.fatal("Player: #{name} has an ID associated that is not an integer!") unless id.kind_of?(Integer)
        @@console.fatal("Player: #{name} has an ID associated that is not valid!") unless id > 0

        if player.key?("region") then
          region = player["region"]

          @@console.fatal("Player: #{name} has a region associated that is not a string!") unless region.kind_of?(String)
          @@console.fatal("Player: #{name} has a region associated that is blank!") if region.empty?

          region = Region.new(name: region)
        end

        @id     = id
        @name   = name
        @region = region || @configuration.region
      end

###############################################################################

    end

###############################################################################

  end

###############################################################################

end

###############################################################################
