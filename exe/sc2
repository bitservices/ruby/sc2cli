#!/usr/bin/env ruby
###############################################################################

require 'sc2cli'

###############################################################################

module SC2Cli

###############################################################################

  console     = Shared::Console.instance
  subcommands = Subcommands.constants.select{|constant| Subcommands.const_get(constant).is_a?(Class)}.to_h{|constant| [constant.to_s.downcase, constant.to_s]}

###############################################################################

  console.info("StarCraft 2 CLI, version: #{VERSION}")
  console.info("Available subcommands: #{subcommands.keys.sort.join(", ")}")
  console.fatal("Usage: #{$0} subcommand [options]") if ARGV.length < 1

###############################################################################

  subcommand = ARGV[0]

  console.fatal("Subcommand cannot be blank!") if subcommand.empty?
  console.fatal("Subcommand not recognised: #{subcommand}!") unless subcommands.key?(subcommand)
  console.info("Subcommand is: #{subcommand}")

  options = ARGV.drop(1)

###############################################################################

  configuration = Shared::Configuration.new

###############################################################################

  runner = Subcommands::const_get(subcommands[subcommand]).new(configuration: configuration, options: options)
  runner.run

###############################################################################

end

###############################################################################
