###############################################################################

module SC2Cli

###############################################################################

  module Shared

###############################################################################

    class Season

###############################################################################

      @@console = Console.instance

###############################################################################

      @@prefix = "/sc2/ladder/season"

###############################################################################

      attr_reader :ends
      attr_reader :id
      attr_reader :number
      attr_reader :region
      attr_reader :starts
      attr_reader :year

###############################################################################

      def initialize(configuration:, region: nil)
        @region = region || configuration.region

        @@console.info("Finding current season information...")

        path = "#{@@prefix}/#{@region.id.to_s}"

        api = Shared::Api.new(configuration: configuration, region: @region)

        result = api.get(path: path)

        parse(json: result)
      end

###############################################################################

      def to_s
        result = String.new

        result += "-------------------------------------------------------------------------------\n"
        result += "Season: #{@id.to_s}\n"
        result += "-------------------------------------------------------------------------------\n"
        result += "Year  : #{@year.to_s}\n"
        result += "Number: #{@number.to_s}\n"
        result += "Began : #{@starts.to_s}\n"
        result += "Ends  : #{@ends.to_s}\n\n"

        return result
      end

###############################################################################

      private

###############################################################################

      def parse(json:)
        @@console.fatal("Returned season information is missing an end date!") unless json.key?("endDate")
        @@console.fatal("Returned season information is missing an ID!") unless json.key?("seasonId")
        @@console.fatal("Returned season information is missing a season number!") unless json.key?("number")
        @@console.fatal("Returned season information is missing a start date!") unless json.key?("startDate")
        @@console.fatal("Returned season information is missing a year!") unless json.key?("year")

        ends = json["endDate"]

        ends = ends.to_i if ends.kind_of?(String)

        @@console.fatal("Returned season information has an end date that is not an integer!") unless ends.kind_of?(Integer)
        @@console.fatal("Returned season information has an end date that is invalid!") unless ends > 0

        id = json["seasonId"]

        @@console.fatal("Returned season information has an ID that is not an integer!") unless id.kind_of?(Integer)
        @@console.fatal("Returned season information has an ID that is invalid!") unless id >= 0

        number = json["number"]

        @@console.fatal("Returned season information has a number that is not an integer!") unless number.kind_of?(Integer)
        @@console.fatal("Returned season information has a number that is invalid!") unless number > 0

        starts = json["startDate"]

        starts = starts.to_i if starts.kind_of?(String)

        @@console.fatal("Returned season information has a start date that is not an integer!") unless starts.kind_of?(Integer)
        @@console.fatal("Returned season information has a start date that is invalid!") unless starts > 0

        year = json["year"]

        @@console.fatal("Returned season information has a year that is not an integer!") unless year.kind_of?(Integer)
        @@console.fatal("Returned season information has a year that is invalid!") unless year > 2000

        @ends   = Time.at(ends)
        @id     = id
        @number = number
        @starts = Time.at(starts)
        @year   = year
      end

###############################################################################

    end

###############################################################################

  end

###############################################################################

end

###############################################################################
