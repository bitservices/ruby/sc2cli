###############################################################################

require 'json'
require 'optparse'

###############################################################################

module SC2Cli

###############################################################################

  module Subcommands

###############################################################################

    class Raw

###############################################################################

      @@console = Shared::Console.instance

###############################################################################

      @configuration
      @path
      @region

###############################################################################

      def initialize(configuration:, options:)
        @configuration = configuration

        path   = nil
        region = nil

        OptionParser.new do |opts|
          opts.banner = "Usage: #{$0} #{self.class.name.split("::").last.downcase} [options]"

          opts.on("-h", "--help", "Prints this help") do
            @@console.info(opts)
            exit
          end

          opts.on("-p", "--path PATH", String, "The API path to query.") do |value|
            path = value
          end

          opts.on("-r", "--region REGION", String, "Region name, such as 'eu' or 'us'. Use configuration region by default.") do |value|
            region = Shared::Region.new(name: value)
          end
        end.parse!

        region ||= @configuration.region

        @@console.fatal("Raw path must be specified!") unless path.kind_of?(String)
        @@console.fatal("Raw path must not be blank!") if path.empty?
        @@console.fatal("Raw path must begin with '/'!") unless path.chars.first == "/"

        @path   = path
        @region = region
      end

###############################################################################

      def run
        @@console.info("Running raw API request:")
        @@console.info(" - Path  : #{@path}")
        @@console.info(" - Region: #{@region.description}")

        api = Shared::Api.new(configuration: @configuration, region: @region)

        result = api.get(path: @path)

        @@console.info(JSON.pretty_generate(result))
      end

###############################################################################

    end

###############################################################################

  end

###############################################################################

end

###############################################################################
